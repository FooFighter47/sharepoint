﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SharePointTaskSerialization
{
    [System.Security.Permissions.ReflectionPermission(System.Security.Permissions.SecurityAction.Assert)]
    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Department { get; set; }
        public string Address { get; set; }

        private string EmployeeID{
            get { return FirstName + LastName; }
        }

        public Employee() { }

        public Employee(string firstName, string lastName, int age, string address, string department)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            Address = address;
            Department = department;
        }
    }
}

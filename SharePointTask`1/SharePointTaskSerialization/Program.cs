﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Reflection;
using System.Configuration;

namespace SharePointTaskSerialization
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>();
            Employee one = new Employee("abc", "dfg", 24, "address", "dept");
            Employee two = new Employee("acb", "fdg", 36, "address", "dept");
            Employee six = new Employee("abc", "dfg", 35, "address", "dept");
            Employee three = new Employee("acf", "bdg", 25, "address", "dept");
            Employee four = new Employee("acb", "fdg", 30, "address", "dept");
            Employee five = new Employee("acf", "bdg", 34, "address", "dept");
            employees.Add(one);
            employees.Add(two);
            employees.Add(three);
            employees.Add(four);
            employees.Add(five);
            employees.Add(six);
            
            string saveLocation = ConfigurationSettings.AppSettings["CommonFile"];
            SerializeEmployees(employees, saveLocation);
            employees = new List<Employee>();
            employees = DeserializeEmployees(saveLocation);
            WriteEmployees(employees);
            Console.ReadKey();

            saveLocation = ConfigurationSettings.AppSettings["SortedFile"];
            Console.WriteLine("Saving all the employees 25-35 years old:");
            List<Employee> filteredEmployees = new List<Employee>();
            foreach(Employee employee in employees)
            {
                if (employee.Age > 25 && employee.Age < 35)
                {
                    filteredEmployees.Add(employee);
                }
            }
            employees.Sort(CompareEmployees);
            WriteEmployees(filteredEmployees);
            SerializeEmployees(employees, "empSorted.xml");
            Console.WriteLine("Save completed");
            Console.ReadKey();
        }

        private static void SerializeEmployees(List<Employee> employees, string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
            Type t = typeof(Employee);
            using (TextWriter write = new StreamWriter(fileName, true))
            {
                XmlSerializer x = new XmlSerializer(typeof(List<Employee>), new XmlRootAttribute("Emloyees"));
                x.Serialize(write, employees);
            }
        }

        private static List<Employee> DeserializeEmployees(string fileName)
        {
            using (TextReader read = new StreamReader(fileName))
            {
                XmlSerializer x = new XmlSerializer(typeof(List<Employee>), new XmlRootAttribute("Emloyees"));
                List<Employee> result = (List<Employee>)x.Deserialize(read);
                return result;
            }
        }

        public static void WriteEmployees(List<Employee> emp)
        {
            for (int i = 0; i < emp.Count; i++)
            {
                var property = typeof(Employee).GetProperty("EmployeeID", BindingFlags.NonPublic | BindingFlags.Instance);
                object id = property.GetValue(emp[i]);// GetValue(e);
                Console.WriteLine("{0} LastName:{1} FirstName:{2} Age:{3} Address:{4} Dapartment:{5} EmployeeID:{6}", i, emp[i].LastName, emp[i].FirstName, emp[i].Age, emp[i].Address, emp[i].Department, id);
            }
        }

        public static int CompareEmployees(Employee a, Employee b)
        {
            var property = typeof(Employee).GetProperty("EmployeeID", BindingFlags.NonPublic | BindingFlags.Instance);
            object idA = property.GetValue(a);
            object idB = property.GetValue(b);
            return (idA.ToString().CompareTo(idB.ToString()));
        }
    }
}

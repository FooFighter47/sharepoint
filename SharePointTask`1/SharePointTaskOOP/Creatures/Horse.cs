﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharePointTaskOOP.Creatures
{
    class Horse : Types.Animal, Interface.IEatHay
    {
        public Horse(int id)
        {
            BeingID = id;
            legs = 4;
        }

        public void EatHay()
        {
            Console.WriteLine("Horse eats hay and feels fine.");
        }
    }
}

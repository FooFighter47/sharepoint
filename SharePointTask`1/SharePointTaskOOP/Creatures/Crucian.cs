﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharePointTaskOOP.Creatures
{
    class Crucian : Types.Fish, Interface.IEatHay
    {
        public Crucian(int id)
        {
            BeingID = id;
        }

        public void EatHay()
        {
            Console.WriteLine("Crucian tries to eat hay and fails. It's still hungry.");
        }
    }
}

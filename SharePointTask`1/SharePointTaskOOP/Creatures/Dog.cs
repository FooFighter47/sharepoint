﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharePointTaskOOP.Creatures
{
    class Dog : Types.Animal, Interface.IBreatheUnderWater
    {
        public Dog(int id)
        {
            BeingID = id;
            legs = 4;
        }
        public void BreatheUnderWater()
        {
            Console.WriteLine("Dog tries to breathe under water and dies.");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharePointTaskOOP.Types
{
    class Animal : Abstract.LivingBeing
    {
        protected int legs;
        public int Legs { get { return legs; } }
    }
}

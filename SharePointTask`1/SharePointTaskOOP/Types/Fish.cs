﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharePointTaskOOP.Types
{
    class Fish : Abstract.LivingBeing, Interface.IBreatheUnderWater
    {
        public void BreatheUnderWater()
        {
            Console.WriteLine("Fish tries to breathe under warter and succeds.");
        }
    }
}

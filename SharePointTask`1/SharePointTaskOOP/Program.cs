﻿using System;
using System.Collections.Generic;
using SharePointTaskOOP.Abstract;
using SharePointTaskOOP.Creatures;
using SharePointTaskOOP.Interface;
using SharePointTaskOOP.Types;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharePointTaskOOP
{
    class Program
    {
        static void Main(string[] args)
        {
            List<LivingBeing> animals = new List<LivingBeing>();
            animals.Add(new Horse(0));
            animals.Add(new Dog(1));
            animals.Add(new Dog(2));
            animals.Add(new Roach(3));
            animals.Add(new Crucian(4));
            animals.Add(new Horse(5));
            Console.WriteLine("Legs total:");
            Console.WriteLine(CountLegs(animals));
            Console.WriteLine("Breath under water:");
            foreach (int i in BreathingUnderWaterIDs(animals))
            {

                Console.WriteLine(i);
            }
            Console.ReadKey();
        }

        public static int CountLegs(List<LivingBeing> beings)
        {
            int totalLegs = 0;
            foreach(LivingBeing a in beings)
            {
                Animal b = a as Animal;
                if (b != null)
                {
                    totalLegs += b.Legs;
                }
            }
            return totalLegs;
        }

        public static List<int> BreathingUnderWaterIDs(ICollection<LivingBeing> beings)
        {
            List<int> CreatureList = new List<int>();
            foreach (LivingBeing a in beings)
            {
                IBreatheUnderWater b = a as IBreatheUnderWater;
                if (b != null)
                {
                    CreatureList.Add(a.BeingID);
                }
            }
            return CreatureList;
        }
    }
}

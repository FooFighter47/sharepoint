﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharePointTaskMultithreading
{
    public class ExceptionEventArgs : EventArgs
    {
        public ExceptionEventArgs(string message)
        {
            Message = message;
        }
        
        public string Message { get; private set; }
    }
}

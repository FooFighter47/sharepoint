﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SharePointTaskMultithreading
{
    //public delegate void ChangeText(object sender, EventArgs e);

    public partial class Form1 : Form
    {
        ExceptionsCreator one;
        ExceptionsCreator two;
        Thread first;
        Thread second;
        static Queue<ExceptionInfo> messages = new Queue<ExceptionInfo>();

        public Form1()
        {
            one = new ExceptionsCreator(messages);
            two = new ExceptionsCreator(messages);
            one.ExceptionGenerated += ExceptionGenerated;
            two.ExceptionGenerated += ExceptionGenerated;
            first = new Thread(one.Generate);
            first.IsBackground = true;
            first.Name = "First thread";
            second = new Thread(two.Generate);
            second.IsBackground = true;
            second.Name = "Second thread";
            first.Start();
            second.Start();
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!one.InProgress)
            {
                one.Start();
                button1.Text = "First thread pause";
            }
            else
            {
                one.Pause();
                button1.Text = "First thread start";
            }
        }

        private void ExceptionGenerated(object sender, EventArgs e)
        {
            Action added = Check;
            try
            {
                Invoke(added);
            }
            catch (ObjectDisposedException x) { }
        }

        public void Check()
        {
            lock (messages)
            {
                var message = messages.Dequeue();
                richTextBox1.Text += String.Format("Thread {0} thrown exception: {1}\r\n", message.Thread, message.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!two.InProgress)
            {
                two.Start();
                button2.Text = "Second thread pause";
            }
            else
            {
                two.Pause();
                button2.Text = "Second thread start";
            }
        }
    }

    public class ExceptionInfo
    {
        public ExceptionInfo(string thread, string message)
        {
            Thread = thread;
            Message = message;
        }

        public string Thread { get; private set; }
        public string Message { get; private set; }
    }
}

﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharePointTaskMultithreading
{
    public class ExceptionsCreator
    {
        Queue<ExceptionInfo> Messages { get; set; }
        public bool InProgress { get; private set; } = false;

        public ExceptionsCreator(Queue<ExceptionInfo> messages)
        {
            Messages = messages;
        }

        public void Generate()
        {
            while(InProgress == true)
            {
                Random rand = new Random();
                Thread.Sleep(rand.Next(50, 150));
                Exception ExceptionToThrow = new OutOfMemoryException();
                try
                {
                    throw ExceptionToThrow;
                }
                catch (Exception ThrownException)
                {
                    lock(Messages)
                    {
                        Messages.Enqueue(new ExceptionInfo(Thread.CurrentThread.Name, ThrownException.ToString()));
                    }
                    if (ExceptionGenerated != null)
                        ExceptionGenerated(this, new EventArgs());
                }
            }
            Thread.Sleep(100);
            Generate();
        }

        public void Start()
        {
            InProgress = true;
        }

        public void Pause()
        {
            InProgress = false;
        }

        public event EventHandler<EventArgs> ExceptionGenerated;
    }
}

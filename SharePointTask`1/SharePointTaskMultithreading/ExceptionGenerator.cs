﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SharePointTaskMultithreading
{
    delegate void ExceptionDelegate();

    class ExceptionGenerator
    {
        private static Thread first;
        private static Thread second;

        public void InitializeThreads(ExceptionDelegate a, ExceptionDelegate b)
        {
            first = new Thread(new ThreadStart(a));
            second = new Thread(new ThreadStart(b));
        }

        public void StartThreads()
        {
            if (first != null && second != null)
            {
                first.Start();
                second.Start();
            }
        }
    }
}

﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SharePointTaskLinqCore
{
    public class RecordManager
    {
        public List<Record> Records { get; private set; }
        private string Address;
        public int NextID { get; private set; }


        public RecordManager(string address)
        {
            if (!File.Exists(String.Concat(address, Properties.Resources.Filename)))
            {
                if (!Directory.Exists(String.Concat(address, "\\pics")))
                {
                    Directory.CreateDirectory(String.Concat(address, "\\pics"));
                }
                Address = address;
                Records = new List<Record>();
            }
            else
            {
                if (!Directory.Exists(String.Concat(address, "\\pics")))
                {
                    Directory.CreateDirectory(String.Concat(address, "\\pics"));
                }
                Address = address;
                Records = DeserializeRecords();
                NextID = 0;
                foreach (Record r in Records)
                {
                    if (r.ID >= NextID)
                    {
                        NextID = r.ID + 1;
                    }
                }
            }
        }

        public void Add(Record record)
        {
            if (record.PictureAddress != String.Empty)
            {
                string pictureAddress = String.Concat(Address, String.Format("\\pics\\{0}.{1}", record.ID, Path.GetExtension(record.PictureAddress)));
                File.Copy(record.PictureAddress, pictureAddress);
                record.PictureAddress = pictureAddress;
            }
            Records.Add(record);
            NextID++;
            SerializeRecords(Records);
        }

        public void Edit(int id, Record record)
        {
            string pictureAddress;
            Record EditedRecord = Records.Find(rec => rec.ID.Equals(id));
            if (!record.PictureAddress.Equals(String.Empty) && !record.PictureAddress.Equals(EditedRecord.PictureAddress))
            {
                pictureAddress = String.Concat(Address, String.Format("\\pics\\{0}.{1}", record.ID, Path.GetExtension(record.PictureAddress)));
                if (File.Exists(pictureAddress))
                {
                    File.Delete(pictureAddress);
                }
                File.Copy(record.PictureAddress, pictureAddress);
                record.PictureAddress = pictureAddress;
            }
            else if (record.PictureAddress.Equals(String.Empty))
            {
                if (!EditedRecord.PictureAddress.Equals(String.Empty))
                {
                    pictureAddress = String.Concat(Address, String.Format("\\pics\\{0}.{1}", EditedRecord.ID, Path.GetExtension(EditedRecord.PictureAddress)));
                    File.Delete(pictureAddress);
                }
            }
            int index = Records.IndexOf(EditedRecord);
            Records.Remove(EditedRecord);
            Records.Insert(index, record);
            SerializeRecords(Records);
        }

        public void Delete(int id)
        {
            Record RecordToDelete = Records.Find(rec => rec.ID.Equals(id));
            if (RecordToDelete.PictureAddress!=String.Empty)
            {
                File.Delete(RecordToDelete.PictureAddress);
            }
            Records.Remove(RecordToDelete);
            SerializeRecords(Records);
        }

        private void SerializeRecords(List<Record> records)
        {
            if (File.Exists(String.Concat(Address, Properties.Resources.Filename)))
                File.Delete(String.Concat(Address, Properties.Resources.Filename));
            Type RecordType = typeof(Record);
            TextWriter write = new StreamWriter(String.Concat(Address, Properties.Resources.Filename), true);
            XmlSerializer serializer = new XmlSerializer(typeof(List<Record>), new XmlRootAttribute("Records"));
            serializer.Serialize(write, Records);
            write.Dispose();
            write.Close();
        }

        private List<Record> DeserializeRecords()
        {
            TextReader read = new StreamReader(String.Concat(Address, "\\Contacts.xml"));
            XmlSerializer x = new XmlSerializer(typeof(List<Record>), new XmlRootAttribute("Records"));
            List<Record> result = (List<Record>)x.Deserialize(read);
            read.Dispose();
            read.Close();
            return result;
        }
    }
}

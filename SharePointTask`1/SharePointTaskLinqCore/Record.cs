﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharePointTaskLinqCore
{
    public class Record
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Group { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string PictureAddress { get; set; }
        

        public Record(string firstName, string lastName, string group, string homePhone, string cellPhone, string picture, int id)
        {
            ID = id;
            FirstName = firstName;
            LastName = lastName;
            Group = group;
            HomePhone = homePhone;
            CellPhone = cellPhone;
            PictureAddress = picture;
        }

        public Record Copy()
        {
            return new Record(FirstName, LastName, Group, HomePhone, CellPhone, PictureAddress, ID);
        }

        public Record()
        { }
        
        public static int CompareTo(Record x, Record y)
        {
            string X = x.FirstName + " " + x.LastName;
            string Y = y.FirstName + " " + y.LastName;
            return X.CompareTo(Y);
        }
    }
}

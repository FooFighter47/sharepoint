﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Security.AccessControl;
using System.Security;
using System.Security.Permissions;
using System.Security.Principal;

namespace SharePointTesk.NET
{
    class TreeReader
    {
        public TreeNode Tree { get; private set; }
        public DirectoryInfo Info { get; private set; }
        //private bool exceptionThrown = false;

        public TreeReader(ref TreeNode node, DirectoryInfo info)
        {
            Tree = node;
            Info = info;
        }

        public void GetDirectories()
        {
            if (Tree.Text.Equals(String.Empty))
            {
                lock(Tree)
                    Tree.Text = Info.Name;
            }
            double size = 0;
            foreach (DirectoryInfo info in Info.GetDirectories())
            {
                try
                {
                    TreeNode temp = new TreeNode(info.Name + " Created:" + info.CreationTime.ToShortDateString());
                    GetDirectories(info, ref temp, ref size);
                    lock (Tree)
                        Tree.Nodes.Add(temp);
                    int index = Tree.Nodes.IndexOf(temp);
                    TreeChanged(this, new EventArgs());
                    Tree.Nodes[index].Name = info.Name + "final";
                    //throw new TimeoutException();
                }
                catch (UnauthorizedAccessException ex)
                {
                    TreeNode temp = new TreeNode(info.Name + " Created:" + info.CreationTime.ToShortDateString());
                    lock (Tree)
                        Tree.Nodes.Add(temp);
                    Tree.Name = info.Name + "final";
                    TreeChanged(this, new EventArgs());
                }
                catch (TimeoutException ex)
                {
                    ExceptionInvoker.ExceptionCreated(this, new ExceptionEventArgs(ex));
                }
            }
            GetFiles(Info, Tree, ref size);
            int dec = 1;
            if (size != 0)
            {
                while (Math.Round(size, dec) == 0)
                    dec++;
            }
            Tree.Text = Tree.Text + " Total:" + Math.Round(size, dec) + " MB";
            GettingDirectoriesEnded(this, new EventArgs());
            TreeChanged(this, new EventArgs());
            Thread.CurrentThread.Join(100);
        }

        private void GetDirectories(DirectoryInfo directory, ref TreeNode node, ref double size)
        {
            double dirSize = 0;
            foreach (DirectoryInfo info in directory.GetDirectories())
            {
                try
                {
                    TreeNode temp = new TreeNode(info.Name + " Created:" + info.CreationTime.ToShortDateString());
                    lock (Tree)
                        node.Nodes.Add(temp);
                    TreeChanged(this, new EventArgs());
                    GetDirectories(info, ref temp, ref dirSize);
                }
                catch (UnauthorizedAccessException ex)
                {
                    TreeNode temp = new TreeNode(info.Name + " Created:" + info.CreationTime.ToShortDateString());
                    lock (Tree)
                        node.Nodes.Add(temp);
                    TreeChanged(this, new EventArgs());
                }
            }
            GetFiles(directory, node, ref dirSize);
            size += dirSize;
            int dec = 1;
            if (dirSize != 0)
            {
                while (Math.Round(dirSize, dec) == 0)
                    dec++;
            }
            node.Text = node.Text + " Size:" + Math.Round(dirSize, dec) + " MB";
            TreeChanged(this, new EventArgs());
        }

        private void GetFiles(DirectoryInfo directory, TreeNode node, ref double size)
        {
            foreach (FileInfo info in directory.GetFiles())
            {
                double fileSize = Convert.ToDouble(info.Length) / 1000000;
                size += fileSize;
                int dec = 1;
                if (fileSize != 0)
                {
                    while (Math.Round(fileSize, dec) == 0)
                        dec++;
                }
                TreeNode temp = new TreeNode(info.Name + " Created:" + info.CreationTime.ToShortDateString() + " Size:" + Math.Round(fileSize, 3) + " MB");
                temp.Name = info.Name + "final";
                lock (Tree)
                    node.Nodes.Add(temp);
                TreeChanged(this, new EventArgs());
            }
        }

        public event EventHandler GettingDirectoriesEnded;
        public event EventHandler TreeChanged;
    }
}

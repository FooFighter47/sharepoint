﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace SharePointTesk.NET
{
    class TreeFiller
    {
        public TreeNode Tree { get; set; }
        public TreeView View { get; private set; }
        public bool TreeChanged = false;
        public bool Final = false;
        private int noChanges = 0;


        public TreeFiller(ref TreeNode node, ref TreeView view)
        {
            Tree = node;
            View = view;
        }

        public void BuildTree()
        {
            try
            {
                if (!TreeChanged)
                    noChanges++;
                else
                {
                    Action<TreeNode> add = AddNew;
                    noChanges = 0;
                    TreeChanged = false;
                    lock (Tree)
                    {
                        View.Invoke(add, Tree);
                    }
                }
                if (noChanges < 10)
                    Thread.Sleep(50);
                else
                {
                    Thread.Sleep(1000);
                    noChanges = 0;
                }
            }
            catch (StackOverflowException e) { }
            catch (TimeoutException ex)
            {
                ExceptionInvoker.ExceptionCreated(this, new ExceptionEventArgs(ex));
            }
            BuildTree();
        }

        private void AddNew(TreeNode source)
        {
            if (View.Nodes.Count == 0)
                View.Nodes.Add(new TreeNode(source.Text));
            foreach (TreeNode node in source.Nodes)
            {
                if (node.Name.EndsWith("final") && View.Nodes[0].Nodes.Find(node.Name, false).Length == 0)
                {
                    View.Nodes[0].Nodes.Add((TreeNode)node.Clone());
                }
            }
            if (Final)
            {
                Final = false;
                View.Nodes[0].Text = source.Text;
            }
        }
    }
}


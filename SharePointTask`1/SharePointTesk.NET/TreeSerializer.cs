﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Threading;
using System.ComponentModel;
using System.Reflection;

namespace SharePointTesk.NET
{
    class TreeSerializer 
    {
        public bool TreeChanged = false;
        public TreeNode Node { get; private set; }
        public string Address;
        private StreamWriter writer;
        private int noChanges = 0;

        public TreeSerializer(ref TreeNode node)
        {
            Node = node;
        }

        public void Serialize()
        {
            try
            {
                if (!TreeChanged)
                    noChanges++;
                else
                {
                    lock (Node)
                    {
                        TreeChanged = false;
                        writer = new StreamWriter(Address.ToString(), false, System.Text.Encoding.UTF8);
                        writer.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                        TreeNode n = new TreeNode();
                        n.Nodes.Add(Node);
                        foreach (TreeNode node in n.Nodes)
                        {
                            saveNode(n.Nodes);
                        }
                        writer.Close();
                    }
                }
                if (noChanges < 10)
                    Thread.Sleep(50);
                else
                {
                    Thread.Sleep(1000);
                    noChanges = 0;
                }
            }
            catch (StackOverflowException e) { }
            catch (TimeoutException ex)
            {
                ExceptionInvoker.ExceptionCreated(this, new ExceptionEventArgs(ex));
            }
            Serialize();
        }

        private void saveNode(TreeNodeCollection collection)
        {
            foreach (TreeNode node in collection)
            {
                if (node.Nodes.Count > 0)
                {
                    writer.WriteLine("<" + node.Text + ">");
                    saveNode(node.Nodes);
                    writer.WriteLine("</" + node.Text + ">");
                }
                else 
                    writer.WriteLine(node.Text);
            }
        }
    }
}

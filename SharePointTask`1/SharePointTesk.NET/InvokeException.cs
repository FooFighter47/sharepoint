﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;

namespace SharePointTesk.NET
{
    public static class ExceptionInvoker
    {
        public static void InvokeException(this ISynchronizeInvoke invoker, Exception ex)
        {
            string thread = Thread.CurrentThread.Name;
            string message = ((Exception)ex).Message;
            MessageBox.Show("Thread '" + thread + "' thrown exception: '" + message + "'.");
        }
        public static EventHandler<ExceptionEventArgs> ExceptionCreated;
    }

    public class ExceptionEventArgs : EventArgs
    {
        public ExceptionEventArgs(Exception ex)
        {
            ExInfo = ex;
        }

        public Exception ExInfo { get; private set; }
    }
}

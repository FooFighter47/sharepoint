﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace SharePointTesk.NET
{

    public partial class Form1 : Form
    {
        private bool directorySelected = false;
        private bool xmlSelected = false;
        private DirectoryInfo topDirectory;
        TreeReader Reader;
        TreeFiller Filler;
        TreeSerializer Serializer;
        Thread Reading;
        Thread Filling;
        Thread Serializing;
        TreeNode node;

        public Form1()
        {
            InitializeComponent();
            node = new TreeNode();
            Filler = new TreeFiller(ref node, ref treeView1);
            Filling = new Thread(Filler.BuildTree);
            Filling.IsBackground = true;
            Filling.Name = "Filling thread";
            Serializer = new TreeSerializer(ref node);
            Serializing = new Thread(Serializer.Serialize);
            Serializing.IsBackground = true;
            Serializing.Name = "Serializing thread";
            Filling.Start();
            Serializing.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (directorySelected && xmlSelected)
            {
                Reading.Start();
                button3.Enabled = false;
                button2.Enabled = false;
                button1.Enabled = false;
            }
            else
            {
                MessageBox.Show("Select directory first!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dial = new FolderBrowserDialog();
            dial.Description = "Select folder to build a tree view";
            dial.ShowNewFolderButton = false;
            if (dial.ShowDialog() == DialogResult.OK)
            {
                topDirectory = new DirectoryInfo(dial.SelectedPath);
                directorySelected = true;
                if (!xmlSelected)
                    label2.Text = "Xml file:";
                label1.Text = "Directory: " + topDirectory.FullName;
            }
            if (directorySelected && xmlSelected)
            {
                Initialize();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog dial = new SaveFileDialog();
            dial.Filter = "Xml file(*.xml)|*.xml";
            dial.InitialDirectory = Environment.CurrentDirectory;
            if (dial.ShowDialog() == DialogResult.OK)
            {
                Serializer.Address = dial.FileName;
                xmlSelected = true;
                if (!directorySelected)
                    label1.Text = "Directory:";
                label2.Text = "Xml file: " + dial.FileName;
            }
            if (directorySelected && xmlSelected)
            {
                Initialize();
            }
        }

        private void Reader_GettingDirectoriesEnded(object sender, EventArgs e)
        {
            directorySelected = false;
            Action enableButtons = () => { button1.Enabled = true; button3.Enabled = true; };
            Invoke(enableButtons);
            Filler.Final = true;
            xmlSelected = false;
            directorySelected = false;
        }

        private void Reader_TreeChanged(object sender, EventArgs e)
        {
            Filler.TreeChanged = true;
            Serializer.TreeChanged = true;
        }

        private void TreadThrownExcetion(object sender, ExceptionEventArgs e)
        {
            this.InvokeException(e.ExInfo);
        }

        private void Initialize()
        {
            if (treeView1.Nodes.Count > 0)
                treeView1.Nodes.Clear();
            node.Nodes.Clear();
            node.Text = String.Empty;
            node.Name = String.Empty;
            Reader = new TreeReader(ref node, topDirectory);
            Reading = new Thread(Reader.GetDirectories);
            Reading.Name = "Reading thread";
            Reader.GettingDirectoriesEnded += Reader_GettingDirectoriesEnded;
            Reader.TreeChanged += Reader_TreeChanged;
            ExceptionInvoker.ExceptionCreated += TreadThrownExcetion;
            button2.Enabled = true;
        }

        private void ShowExceptionMessage(string thread, string message)
        {
            MessageBox.Show("Thread '" + thread + "' thrown exception: '" + message + "'.");
        }
    }
}

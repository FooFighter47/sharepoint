﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SharePointTaskLinqCore;
using System.Text.RegularExpressions;

namespace SharePointTaskLinqUI
{
    public partial class Form1 : Form
    {
        bool Edit = false;
        RecordManager manager;
        private static Record OpenedRecord;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            manager = new RecordManager(Environment.CurrentDirectory);
            listBox1.DataSource = manager.Records.Select(r => String.Concat(r.FirstName, " ", r.LastName)).ToList();
            listBox1.SelectedIndex = -1;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //Select picture for record
            OpenFileDialog dial = new OpenFileDialog();
            dial.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF";
            dial.InitialDirectory = Environment.CurrentDirectory;

            if (dial.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (dial.FileName != null)
                    {
                        var i = Image.FromFile(dial.FileName);
                        pictureBox.Image = i;
                        pictureBox.ImageLocation = dial.FileName;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            //Add new record
            if (Edit && listBox1.Visible)
            {
                OpenedRecord = null;
                listBox1.SelectedIndex = -1;
                Edit = false;
            }
            else if (Edit && treeView1.Visible)
            {
                OpenedRecord = null;
                treeView1.SelectedNode = null;
                treeView1.Select();
                firstNameTextBox.Text = "";
                secondNameTextBox.Text = "";
                groupTextBox.Text = "";
                homePhoneTextBox.Text = "";
                cellPhoneTextBox.Text = "";
                pictureBox.Image = null;
                pictureBox.ImageLocation = null;
                pictureBox.Invalidate();
                pictureBox.Update();
                Edit = false;
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Open record
            if (listBox1.SelectedIndex >= 0)
            {
                var rec = manager.Records.ToList();
                if (filterCheckBox.Checked)
                {
                    rec.Sort(Record.CompareTo);
                }
                OpenedRecord = rec[listBox1.SelectedIndex].Copy();
                firstNameTextBox.Text = OpenedRecord.FirstName;
                secondNameTextBox.Text = OpenedRecord.LastName;
                groupTextBox.Text = OpenedRecord.Group;
                homePhoneTextBox.Text = OpenedRecord.HomePhone;
                cellPhoneTextBox.Text = OpenedRecord.CellPhone;
                if (!OpenedRecord.PictureAddress.Equals(String.Empty))
                {
                    FileStream stream = new FileStream(OpenedRecord.PictureAddress, FileMode.Open, FileAccess.Read);
                    pictureBox.Image = Image.FromStream(stream);
                    pictureBox.ImageLocation = OpenedRecord.PictureAddress;
                    stream.Close();
                }
                else
                {
                    pictureBox.Image = null;
                    pictureBox.ImageLocation = null;
                }
                Edit = true;
            }
            else
            {
                OpenedRecord = null;
                firstNameTextBox.Text = "";
                secondNameTextBox.Text = "";
                groupTextBox.Text = "";
                homePhoneTextBox.Text = "";
                cellPhoneTextBox.Text = "";
                pictureBox.Image = null;
                pictureBox.ImageLocation = null;
                pictureBox.Invalidate();
                pictureBox.Update();
                Edit = false;
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            //Delete
            if (Edit == true)
            {
                manager.Delete(OpenedRecord.ID);
                OpenedRecord = null;
                Edit = false;
                ReloadRecords();
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            //Save
            if(!ParseFields())
            {
                return;
            }
            if (Edit)
            {
                OpenedRecord.FirstName = firstNameTextBox.Text;
                OpenedRecord.LastName = secondNameTextBox.Text;
                OpenedRecord.Group = groupTextBox.Text;
                OpenedRecord.HomePhone = homePhoneTextBox.Text;
                OpenedRecord.CellPhone = cellPhoneTextBox.Text;
                if (pictureBox.ImageLocation != null)
                    OpenedRecord.PictureAddress = pictureBox.ImageLocation;
                else
                    OpenedRecord.PictureAddress = String.Empty;
                manager.Edit(OpenedRecord.ID, OpenedRecord);
                ReloadRecords();
            }
            else
            {
                if (pictureBox.ImageLocation != null)
                {
                    OpenedRecord = new Record(firstNameTextBox.Text, secondNameTextBox.Text, groupTextBox.Text, homePhoneTextBox.Text, cellPhoneTextBox.Text, pictureBox.ImageLocation, manager.NextID);
                }
                else
                {
                    OpenedRecord = new Record(firstNameTextBox.Text, secondNameTextBox.Text, groupTextBox.Text, homePhoneTextBox.Text, cellPhoneTextBox.Text, String.Empty, manager.NextID);
                }
                manager.Add(OpenedRecord);
                ReloadRecords();
            }
        }

        private void groupCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (groupCheckBox.Checked)
            {
                listBox1.Visible = false;
                treeView1.Visible = true;
            }
            else
            {
                listBox1.Visible = true;
                treeView1.Visible = false;
            }
            ReloadRecords();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView1.SelectedNode != null && treeView1.SelectedNode.Level > 0)
            {
                OpenedRecord = manager.Records.Find(r => r.ID.Equals(Convert.ToInt32(treeView1.SelectedNode.Name))).Copy();
                firstNameTextBox.Text = OpenedRecord.FirstName;
                secondNameTextBox.Text = OpenedRecord.LastName;
                groupTextBox.Text = OpenedRecord.Group;
                homePhoneTextBox.Text = OpenedRecord.HomePhone;
                cellPhoneTextBox.Text = OpenedRecord.CellPhone;
                if (!OpenedRecord.PictureAddress.Equals(String.Empty))
                {
                    FileStream stream = new FileStream(OpenedRecord.PictureAddress, FileMode.Open, FileAccess.Read);
                    pictureBox.Image = Image.FromStream(stream);
                    pictureBox.ImageLocation = OpenedRecord.PictureAddress;
                    stream.Close();
                    //pictureBox1.Image = Image.FromFile(OpenedRecord.PictureAddress);
                    //pictureBox1.ImageLocation = OpenedRecord.PictureAddress;
                    pictureBox.Refresh();
                }
                else
                {
                    pictureBox.Image = null;
                    pictureBox.ImageLocation = null;
                    pictureBox.Refresh();
                }
                Edit = true;
            }
            else if(treeView1.SelectedNode != null && treeView1.SelectedNode.Level != 0)
            {
                OpenedRecord = null;
                firstNameTextBox.Text = "";
                secondNameTextBox.Text = "";
                groupTextBox.Text = "";
                homePhoneTextBox.Text = "";
                cellPhoneTextBox.Text = "";
                pictureBox.Image = null;
                pictureBox.ImageLocation = null;
                pictureBox.Refresh();
                Edit = false;
            }
        }

        private void filterCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            ReloadRecords();
        }

        private bool ParseFields()
        {
            string expression = "[а-я0-9a-z]";
            if (!Regex.IsMatch(firstNameTextBox.Text, expression))
            {
                MessageBox.Show("Все поля должны быть заполнены!");
                return false;
            }
            if (!Regex.IsMatch(secondNameTextBox.Text, expression))
            {
                MessageBox.Show("Все поля должны быть заполнены!");
                return false;
            }
            if (!Regex.IsMatch(groupTextBox.Text, expression))
            {
                MessageBox.Show("Все поля должны быть заполнены!");
                return false;
            }
            expression = @"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$";
            if (!Regex.IsMatch(homePhoneTextBox.Text, expression))
            {
                MessageBox.Show("Все поля должны быть заполнены!");
                return false;
            }
            if (!Regex.IsMatch(cellPhoneTextBox.Text, expression))
            {
                MessageBox.Show("Все поля должны быть заполнены!");
                return false;
            }
            return true;
        }

        private void ReloadRecords()
        {
            if (listBox1.Visible)
            {
                List<Record> rec;
                List<string> list;
                list = manager.Records.Select(r => String.Concat(r.FirstName, " ", r.LastName)).ToList();
                if (filterCheckBox.Checked)
                {
                    list.Sort();
                    rec = manager.Records.ToList();
                    rec.Sort(Record.CompareTo);
                    int index = -1;
                    if (OpenedRecord != null)
                    {
                        index = rec.IndexOf(OpenedRecord);
                    }
                    listBox1.DataSource = list;
                    listBox1.SelectedIndex = index;
                }
                else
                {
                    int index = -1;
                    if (OpenedRecord != null)
                    {
                        index = manager.Records.ToList().IndexOf(OpenedRecord);
                    }
                    listBox1.DataSource = list;
                    if (list.Count != 0)
                    {
                        listBox1.SelectedIndex = index;
                    }
                    else
                    {
                        OpenedRecord = null;
                        firstNameTextBox.Text = "";
                        secondNameTextBox.Text = "";
                        groupTextBox.Text = "";
                        homePhoneTextBox.Text = "";
                        cellPhoneTextBox.Text = "";
                        pictureBox.Image = null;
                        pictureBox.ImageLocation = null;
                        pictureBox.Invalidate();
                        pictureBox.Update();
                        Edit = false;
                    }
                }
            }
            else if (treeView1.Visible)
            {
                treeView1.Nodes.Clear();
                List<string> Groups = manager.Records.Select(r => r.Group).Distinct().ToList();
                foreach (string r in Groups)
                {
                    treeView1.Nodes.Add(r);
                }
                foreach (TreeNode n in treeView1.Nodes)
                {
                    var r = manager.Records.Where(re => re.Group.Equals(n.Text)).ToList();
                    if (filterCheckBox.Checked)
                    {
                        r.Sort(Record.CompareTo);
                    }
                    foreach (Record re in r)
                    {
                        n.Nodes.Add(re.ID.ToString(), re.FirstName + " " + re.LastName);
                    }
                }
                if (OpenedRecord != null)
                {
                    var node = treeView1.Nodes.Find(OpenedRecord.ID.ToString(), true);
                    treeView1.SelectedNode = node[0];
                    treeView1.Select();
                }
                else
                {
                    firstNameTextBox.Text = "";
                    secondNameTextBox.Text = "";
                    groupTextBox.Text = "";
                    homePhoneTextBox.Text = "";
                    cellPhoneTextBox.Text = "";
                    pictureBox.Image = null;
                    pictureBox.ImageLocation = null;
                    pictureBox.Invalidate();
                    pictureBox.Update();
                    Edit = false;
                }
            }
        }
    }
}

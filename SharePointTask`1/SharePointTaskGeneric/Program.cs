﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharePointTaskGeneric
{
    class Program
    {
        static void Main(string[] args)
        {
            CompareSortingString(10000, 100);
            Console.WriteLine("Multiplication table via foreach:");
            TableViaForeach(10);
            Console.ReadKey(false);
            Console.WriteLine("Multiplication table via for:");
            TableViaFor(10);
            Console.ReadKey(false);
            Console.WriteLine("Multiplication table via while:");
            TableViaWhile(10);
            Console.ReadKey(false);
            Console.WriteLine("Multiplication table via do...while:");
            TableViaDoWhile(10);
            Console.ReadKey(false);
            AdaptiveList<string> list = new AdaptiveList<string>();
            list.Add("bla0");
            list.Add("bla1");
            list.Add("bla2");
            list.Add("bla3");
            list.Add("bla4");
            list.Add("bla5");
            list.Add("bla6");
            list.Add("bla7");
            CompareAddingInt(10000, 100);
            CompareAddingString(10000, 100);
            CompareGettingInt(10000, 100);
            CompareGettingString(10000, 100);
            CompareSortingInt(10000, 100);
            CompareSortingString(10000, 100);
            List<NonNullableGeneric<int>> genList = new List<NonNullableGeneric<int>>();
            for (int i = 1; i < 10; i++)
            {
                NonNullableGeneric<int> gen = new NonNullableGeneric<int>(i);
                genList.Add(gen);
            }
            genList.Sort();
            foreach (NonNullableGeneric<int> a in genList)
            {
                Console.WriteLine(a.Property);
            }
            Console.ReadKey();
            //NonNullableGeneric<int?> fail = new NonNullableGeneric<int?>(null);
        }

        #region Comparations

        public static void CompareAddingInt(int elements, int iterations)
        {
            List<int> list = new List<int>();
            ArrayList arList = new ArrayList();
            Stopwatch watch = new Stopwatch();
            int listTime = 0;
            int arListTime = 0;
            Console.WriteLine("Let us try what is faster for adding {0} int elements for {1} iterations!", elements, iterations);
            for (int i = 0; i < iterations; i++)
            {
                watch.Start();
                for (int j = 0; j < elements; j++)
                {
                    list.Add(j);
                }
                watch.Stop();
                listTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            listTime = listTime / iterations;
            for (int i = 0; i < iterations; i++)
            {
                watch.Start();
                for (int j = 0; j < elements; j++)
                {
                    arList.Add(j);
                }
                watch.Stop();
                arListTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            arListTime = arListTime / iterations;
            Console.WriteLine("Average list adding time is " + listTime + " ticks.");
            Console.WriteLine("Average arrayList adding time is " + arListTime + " ticks.");
            Console.ReadKey();
        }

        public static void CompareAddingString(int elements, int iterations)
        {
            List<string> list = new List<string>();
            ArrayList arList = new ArrayList();
            Stopwatch watch = new Stopwatch();
            int listTime = 0;
            int arListTime = 0;
            Console.WriteLine("Let us try what is faster for adding {0} string elements for {1} iterations!", elements, iterations);
            for (int i = 0; i < iterations; i++)
            {
                string toAdd = "Add me!";
                watch.Start();
                for (int j = 0; j < elements; j++)
                {
                    list.Add(toAdd);
                }
                watch.Stop();
                listTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            listTime = listTime / iterations;
            for (int i = 0; i < iterations; i++)
            {
                string toAdd = "Add me!";
                watch.Start();
                for (int j = 0; j < elements; j++)
                {
                    arList.Add(toAdd);
                }
                watch.Stop();
                arListTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            arListTime = arListTime / iterations;
            Console.WriteLine("Average list adding time is " + listTime + " ticks.");
            Console.WriteLine("Average arrayList adding time is " + arListTime + " ticks.");
            Console.ReadKey();
        }

        public static void CompareGettingInt(int elements, int iterations)
        {
            List<int> list = new List<int>();
            ArrayList arList = new ArrayList();
            Stopwatch watch = new Stopwatch();
            int listTime = 0;
            int arListTime = 0;
            for (int i = 0; i < elements; i++)
            {
                list.Add(i);
                arList.Add(i);
            }
            Console.WriteLine("Let us try what is faster for getting {0} int elements for {1} iterations!", elements, iterations);
            for (int i = 0; i < iterations; i++)
            {
                int result;
                watch.Start();
                for (int j = 0; j < elements; j++)
                {
                    result = list[j];
                }
                watch.Stop();
                listTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            listTime = listTime / iterations;
            for (int i = 0; i < iterations; i++)
            {
                int result;
                watch.Start();
                for (int j = 0; j < elements; j++)
                {
                    result = (int)arList[j];
                }
                watch.Stop();
                arListTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            arListTime = arListTime / iterations;
            Console.WriteLine("Average list getting time is " + listTime + " ticks.");
            Console.WriteLine("Average arrayList getting time is " + arListTime + " ticks.");
            Console.ReadKey();
        }

        public static void CompareGettingString(int elements, int iterations)
        {
            List<string> list = new List<string>();
            ArrayList arList = new ArrayList();
            Stopwatch watch = new Stopwatch();
            int listTime = 0;
            int arListTime = 0;
            for (int i = 0; i < elements; i++)
            {
                list.Add(i.ToString());
                arList.Add(i.ToString());
            }
            Console.WriteLine("Let us try what is faster for getting {0} string elements for {1} iterations!", elements, iterations);
            for (int i = 0; i < iterations; i++)
            {
                string result;
                watch.Start();
                for (int j = 0; j < elements; j++)
                {
                    result = list[j];
                }
                watch.Stop();
                listTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            listTime = listTime / iterations;
            for (int i = 0; i < iterations; i++)
            {
                string result;
                watch.Start();
                for (int j = 0; j < elements; j++)
                {
                    result = (string)arList[j];
                }
                watch.Stop();
                arListTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            arListTime = arListTime / iterations;
            Console.WriteLine("Average list getting time is " + listTime + " ticks.");
            Console.WriteLine("Average arrayList getting time is " + arListTime + " ticks.");
            Console.ReadKey();
        }

        public static void CompareSortingInt(int elements, int iterations)
        {
            List<int> list = new List<int>();
            ArrayList arList = new ArrayList();
            Random rand = new Random();
            Stopwatch watch = new Stopwatch();
            int listTime = 0;
            int arListTime = 0;
            for (int i = 0; i < elements; i++)
            {
                list.Add(rand.Next());
                arList.Add(rand.Next());
            }
            Console.WriteLine("Let us try what is faster for sorting {0} int elements for {1} iterations!", elements, iterations);
            for (int i = 0; i < iterations; i++)
            {
                watch.Start();
                list.Sort();
                watch.Stop();
                listTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            listTime = listTime / iterations;
            for (int i = 0; i < iterations; i++)
            {
                watch.Start();
                arList.Sort();
                watch.Stop();
                arListTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            arListTime = arListTime / iterations;
            Console.WriteLine("List sorting time is " + listTime + " ticks.");
            Console.WriteLine("ArrayList sorting time is " + arListTime + " ticks.");
            Console.ReadKey();
        }

        public static void CompareSortingString(int elements, int iterations)
        {
            List<string> list = new List<string>();
            ArrayList arList = new ArrayList();
            Random rand = new Random();
            Stopwatch watch = new Stopwatch();
            int listTime = 0;
            int arListTime = 0;
            for (int i = 0; i < elements; i++)
            {
                list.Add(rand.Next().ToString());
                arList.Add(rand.Next().ToString());
            }
            Console.WriteLine("Let us try what is faster for sorting {0} string elements for {1} iterations!", elements, iterations);
            for (int i = 0; i < iterations; i++)
            {
                watch.Start();
                list.Sort();
                watch.Stop();
                listTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            listTime = listTime / iterations;
            for (int i = 0; i < iterations; i++)
            {
                watch.Start();
                arList.Sort();
                watch.Stop();
                arListTime += (int)watch.ElapsedTicks;
                watch.Reset();
            }
            arListTime = arListTime / iterations;
            Console.WriteLine("List sorting time is " + listTime + " ticks.");
            Console.WriteLine("ArrayList sorting time is " + arListTime + " ticks.");
            Console.ReadKey();
        }

        #endregion

        #region MultiplicationTables

        public static void TableViaFor(int size)
        {
            int maxlength = size * size;
            maxlength = (int)Math.Floor(Math.Log10(maxlength) + 1);
            for (int multA = 1; multA <= size; multA++)
            {
                for (int multB = 1; multB <= size; multB++)
                {
                    BuildTable(multA, multB, size, maxlength);
                }
            }
        }

        public static void TableViaWhile(int size)
        {
            int maxlength = size * size;
            maxlength = (int)Math.Floor(Math.Log10(maxlength) + 1);
            int multA = 0;
            int multB = 0;
            while (multA <= size)
            {
                multA++;
                while (multB < size)
                {
                    multB++;
                    BuildTable(multA, multB, size, maxlength);
                }
                multB = 0;
            }
        }

        public static void TableViaDoWhile(int size)
        {
            int maxlength = size * size;
            maxlength = (int)Math.Floor(Math.Log10(maxlength) + 1);
            int multA = 0;
            int multB = 0;
            do
            {
                multA++;
                do
                {
                    multB++;
                    BuildTable(multA, multB, size, maxlength);
                }
                while (multB < size);
                multB = 0;
            }
            while (multA < size);
        }

        public static void TableViaForeach(int size)
        {
            int maxlength = size * size;
            maxlength = (int)Math.Floor(Math.Log10(maxlength) + 1);
            List<int> mult = new List<int>();
            mult.AddRange(Enumerable.Range(1, size));
            while (mult.Last() < size)
            {
                mult.Add(mult.Last() + 1);
            }
            foreach (int multA in mult)
            {
                foreach (int multB in mult)
                {
                    BuildTable(multA, multB, size, maxlength);
                }
            }
        }

        public static void BuildTable(int multA, int multB, int size, int maxlength)
        {
            string value = "";
            if (multA == 1 && multB == 1)
            {
                while (value.Length <= maxlength)
                    value += " ";
            }
            else
            {
                value = Convert.ToString(multA * multB);
                while (value.Length <= maxlength)
                    value += " ";
            }
            if (multA == multB)
                Console.ForegroundColor = ConsoleColor.Red;
            if (multB < size)
            {
                Console.Write(value);
            }
            else
            {
                Console.WriteLine(value);
            }
            Console.ResetColor();
        }

        #endregion

        #region Generics

        public class AdaptiveList<T> : IList<T>
        {
            private List<T> ShortList = new List<T>();
            private SortedList<T, T> LongList;

            public IList<T> List
            {
                get
                {
                    if (LongList != null)
                        return LongList.Values;
                    else
                        return ShortList;
                }
            }

            public int Count
            {
                get
                {
                    if (ShortList != null)
                        return ((IList<T>)ShortList).Count;
                    else
                        return ((IList<T>)LongList).Count;
                }
            }

            public bool IsReadOnly
            {
                get
                {
                    if (ShortList != null)
                        return ((IList<T>)ShortList).IsReadOnly;
                    else
                        return ((IList<T>)LongList).IsReadOnly;
                }
            }

            public T this[int index]
            {
                get
                {
                    if (ShortList != null)
                        return ((IList<T>)ShortList)[index];
                    else
                        return ((IList<T>)LongList.Values)[index];
                }

                set
                {
                    if (ShortList != null)
                        ((IList<T>)ShortList)[index] = value;
                    else
                        ((IList<T>)LongList)[index] = value;
                }
            }

            public void Add(T item)
            {
                if (ShortList != null && ShortList.Count < 5)
                {
                    ShortList.Add(item);
                }
                else if (ShortList != null && ShortList.Count == 5)
                {
                    LongList = new SortedList<T, T>();
                    foreach (T listItem in ShortList)
                    {
                        LongList.Add(listItem, listItem);
                    }
                    LongList.Add(item, item);
                    ShortList = null;
                }
                else
                {
                    LongList.Add(item, item);
                }
            }

            public void Insert(int index, T item)
            {
                if (ShortList != null && ShortList.Count < 5)
                {
                    ShortList.Insert(index, item);
                }
                else if (ShortList != null && ShortList.Count == 5)
                {
                    LongList = new SortedList<T, T>();
                    foreach (T listItem in ShortList)
                    {
                        LongList.Add(listItem, listItem);
                    }
                    ShortList = null;
                }
                else
                {
                    LongList.Add(item, item);
                }
            }

            public int IndexOf(T item)
            {
                if (ShortList != null)
                    return ((IList<T>)ShortList).IndexOf(item);
                else
                    return ((IList<T>)LongList.Values).IndexOf(item);
            }

            public void RemoveAt(int index)
            {
                if (ShortList != null)
                    ((IList<T>)ShortList).RemoveAt(index);
                else
                    ((IList<T>)LongList.Values).RemoveAt(index);
            }

            public void Clear()
            {
                if (ShortList != null)
                    ((IList<T>)ShortList).Clear();
                else
                    ((IList<T>)LongList.Values).Clear();
            }

            public bool Contains(T item)
            {
                if (ShortList != null)
                    return ((IList<T>)ShortList).Contains(item);
                else
                    return ((IList<T>)LongList.Values).Contains(item);
            }

            public void CopyTo(T[] array, int arrayIndex)
            {
                if (ShortList != null)
                    ((IList<T>)ShortList).CopyTo(array, arrayIndex);
                else
                    ((IList<T>)LongList.Values).CopyTo(array, arrayIndex);
            }

            bool ICollection<T>.Remove(T item)
            {
                if (LongList != null && LongList.Count > 6)
                {
                    return ShortList.Remove(item);
                }
                else if (LongList != null && LongList.Count == 6)
                {
                    ShortList = new List<T>();
                    foreach (T listItem in LongList.Values)
                    {
                        ShortList.Add(listItem);
                    }
                    LongList = null;
                    return ShortList.Remove(item);
                }
                else
                {
                    return ShortList.Remove(item);
                }
            }

            public IEnumerator<T> GetEnumerator()
            {
                if (ShortList != null)
                    return ((IList<T>)ShortList).GetEnumerator();
                else
                    return ((IList<T>)LongList.Values).GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                if (ShortList != null)
                    return ((IList<T>)ShortList).GetEnumerator();
                else
                    return ((IList<T>)LongList.Values).GetEnumerator();
            }
        }

        public class NonNullableGeneric<T> : IComparable<NonNullableGeneric<T>> where T : struct, IComparable
        {
            private T property;
            public T Property { get { return property; } set { property = value; } }

            public NonNullableGeneric(T prop)
            {
                this.property = prop;
            }

            public int CompareTo(NonNullableGeneric<T> other)
            {
                return property.CompareTo(other.Property);
            }
        }

        #endregion

    }
}

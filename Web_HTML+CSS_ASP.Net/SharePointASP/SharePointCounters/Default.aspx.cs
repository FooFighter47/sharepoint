﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SharePointCounters
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Table MainList = new Table();
            Form.Controls.Add(MainList);

            TableRow Row = new TableRow();
            Row.ID = "NickRow";
            TableCell cell = new TableCell();
            Label Label = new Label();
            Label.Text = "Общая статистика";
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            string path = Server.MapPath(ConfigurationManager.AppSettings["General"]);
            int Users = Global.GetTotalUsers(path);
            int TodayUsers = Global.GetTodayUsers(path);
            int Requests = Global.GetTotalRequests(path);
            int TodayRequests = Global.GetTodayRequests(path);

            Row = new TableRow();
            Row.ID = "NickRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "Количество посетителей: " + Users;
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Row = new TableRow();
            Row.ID = "NickRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "Количество посетителей сегодня: " + TodayUsers;
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Row = new TableRow();
            Row.ID = "NickRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "Количество запросов: " + Requests;
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Row = new TableRow();
            Row.ID = "NickRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "Количество запросов сегодня: " + TodayRequests;
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Row = new TableRow();
            Row.ID = "NickRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "";
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            path = Server.MapPath(ConfigurationManager.AppSettings["Pages"]);
            List<string> Pages = File.ReadLines(path).ToList();

            foreach (string page in Pages)
            {
                path = Server.MapPath("/SharePointCounters/PagesStatistics/" + page + ".txt");
                if (File.Exists(path))
                {
                    Users = Global.GetTotalUsers(path);
                    TodayUsers = Global.GetTodayUsers(path);
                    Requests = Global.GetTotalRequests(path);
                    TodayRequests = Global.GetTodayRequests(path);

                    Row = new TableRow();
                    Row.ID = "NickRow";
                    cell = new TableCell();
                    Label = new Label();
                    Label.Text = "Статистика для " + page;
                    cell.Controls.Add(Label);
                    Row.Cells.Add(cell);
                    MainList.Rows.Add(Row);

                    Row = new TableRow();
                    Row.ID = "NickRow";
                    cell = new TableCell();
                    Label = new Label();
                    Label.Text = "Количество посетителей: " + Users;
                    cell.Controls.Add(Label);
                    Row.Cells.Add(cell);
                    MainList.Rows.Add(Row);

                    Row = new TableRow();
                    Row.ID = "NickRow";
                    cell = new TableCell();
                    Label = new Label();
                    Label.Text = "Количество посетителей сегодня: " + TodayUsers;
                    cell.Controls.Add(Label);
                    Row.Cells.Add(cell);
                    MainList.Rows.Add(Row);

                    Row = new TableRow();
                    Row.ID = "NickRow";
                    cell = new TableCell();
                    Label = new Label();
                    Label.Text = "Количество запросов: " + Requests;
                    cell.Controls.Add(Label);
                    Row.Cells.Add(cell);
                    MainList.Rows.Add(Row);

                    Row = new TableRow();
                    Row.ID = "NickRow";
                    cell = new TableCell();
                    Label = new Label();
                    Label.Text = "Количество запросов сегодня: " + TodayRequests;
                    cell.Controls.Add(Label);
                    Row.Cells.Add(cell);
                    MainList.Rows.Add(Row);

                    Row = new TableRow();
                    Row.ID = "NickRow";
                    cell = new TableCell();
                    Label = new Label();
                    Label.Text = "";
                    cell.Controls.Add(Label);
                    Row.Cells.Add(cell);
                    MainList.Rows.Add(Row);
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Security.AccessControl;
using System.Security.Principal;

namespace SharePointCounters
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            string Pages = Server.MapPath(ConfigurationManager.AppSettings["Pages"]);
            if (Pages != null && !File.Exists(Pages))
                File.Create(Pages);
        }

        void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            if (!Request.PhysicalPath.Contains("__browserLink\\requestData"))
            {
                string path = Server.MapPath(ConfigurationManager.AppSettings["General"]);
                UsersInfoUpdate(path);
                path = Path.GetFileNameWithoutExtension(Request.PhysicalPath);
                if (!Directory.Exists(Server.MapPath("/SharePointCounters/PagesStatistics/")))
                    Directory.CreateDirectory(Server.MapPath("/SharePointCounters/PagesStatistics/"));
                path = Server.MapPath("/SharePointCounters/PagesStatistics/" + path + ".txt");
                UsersInfoUpdate(path);
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!Request.PhysicalPath.Contains("__browserLink\\requestData"))
            {
                string path = Server.MapPath(ConfigurationManager.AppSettings["General"]);
                RequestsInfoUpdate(path);
                path = Path.GetFileNameWithoutExtension(Request.PhysicalPath);
                path = Server.MapPath("/SharePointCounters/PagesStatistics/" + path + ".txt");
                RequestsInfoUpdate(path);
            }
        }

        private void UsersInfoUpdate(string path)
        {
            List<string> Data;
            string ID = "";
            if (Session!=null)
                ID = Session.SessionID;
            else if(HttpContext.Current.Session != null)
                ID = HttpContext.Current.Session.SessionID;
            if (File.Exists(path) && !ID.Equals(String.Empty))
            {
                Data = File.ReadLines(path).ToList();
                if (!File.GetLastWriteTime(path).Date.Equals(DateTime.Now.Date))
                {
                    for (int i = 3; i < Data.Count; i++)
                    {
                        if (Data[i].EndsWith("today"))
                        {
                            Data[i] = Data[i].Replace("today", "");
                        }
                    }
                }
                if (Data.Count > 2)
                {
                    bool NewUser = true;
                    for (int i = 3; i < Data.Count; i++)
                    {
                        if (Data[i].Contains(ID))
                        {
                            NewUser = false;
                            if (!Data[i].EndsWith("today"))
                            {
                                Data[i] = ID + " today";
                            }
                        }
                    }
                    if (NewUser)
                    {
                        Data.Insert(3, ID + " today");
                    }
                }
                else if (Data.Count == 0)
                {
                    Data.Add("Requests: 0");
                    Data.Add("TodayRequests: 0");
                    Data.Add("Users:");
                    Data.Add(ID + " today");
                }
                else
                {
                    Data.Add("Users:");
                    Data.Add(ID + " today");
                }
            }
            else
            {
                Data = new List<string>();
                Data.Add("Requests: 0");
                Data.Add("TodayRequests: 0");
                Data.Add("Users:");
                Data.Add(ID + " today");
            }
            File.WriteAllLines(path, Data);
        }

        private void RequestsInfoUpdate(string path)
        {
            List<string> Data;// = File.ReadLines(path).ToList();
            if (File.Exists(path))
            {
                Data = File.ReadLines(path).ToList();
                if (Data.Count != 0)
                {
                    if (!File.GetLastWriteTime(path).Date.Equals(DateTime.Now.Date))
                        Data[1] = "TodayRequests: 0";
                    int length = Data[0].Length - 1;
                    int total = 0;
                    while (Int32.TryParse(Data[0].Substring(length), out total))
                    {
                        length--;
                    }
                    length++;
                    total = Convert.ToInt32(Data[0].Substring(length));
                    total++;

                    length = Data[1].Length - 1;
                    int today = 0;
                    while (int.TryParse(Data[1].Substring(length), out today))
                    {
                        length--;
                    }
                    length++;
                    today = Convert.ToInt32(Data[1].Substring(length));
                    today++;

                    Data[0] = "Requests: " + total;
                    Data[1] = "TodayRequests: " + today;
                }
                else
                {
                    Data = new List<string>();
                    Data.Add("Requests: 1");
                    Data.Add("TodayRequests: 1");
                }
            }
            else
            {
                Data = new List<string>();
                Data.Add("Requests: 1");
                Data.Add("TodayRequests: 1");
            }
            File.WriteAllLines(path, Data);
        }

        public static int GetTodayRequests(string path)
        {
            List<string> Data = File.ReadLines(path).ToList();
            int requests = 0;
            if (Data != null && Data.Count != 0)
            {
                int length = Data[0].Length - 1;
                while (int.TryParse(Data[0].Substring(length), out requests))
                {
                    length--;
                }
                length++;
                requests = Convert.ToInt32(Data[0].Substring(length));
            }
            return requests;
        }

        public static int GetTotalRequests(string path)
        {
            List<string> Data = File.ReadLines(path).ToList();
            int requests = 0;
            if (Data != null && Data.Count != 0)
            {
                int length = Data[1].Length - 1;
                while (int.TryParse(Data[1].Substring(length), out requests))
                {
                    length--;
                }
                length++;
                requests = Convert.ToInt32(Data[1].Substring(length));
            }
            return requests;
        }

        public static int GetTodayUsers(string path)
        {
            List<string> Data = File.ReadLines(path).ToList();
            int users = 0;
            if (Data != null && Data.Count != 0)
            {
                foreach (string line in Data)
                {
                    if (line.EndsWith("today"))
                    {
                        users++;
                    }
                }
            }
            return users;
        }

        public static int GetTotalUsers(string path)
        {
            List<string> Data = File.ReadLines(path).ToList();
            int users = 0;
            if (Data != null && Data.Count != 0)
            {
                for (int i = 3; i < Data.Count; i++)
                {
                    if (!Data[i].Equals(String.Empty))
                    {
                        users++;
                    }
                }
            }
            return users;
        }
    }
}
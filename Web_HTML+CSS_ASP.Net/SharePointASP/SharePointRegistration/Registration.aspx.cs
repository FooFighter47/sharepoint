﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SharePointRegistration
{
    public partial class Registration : System.Web.UI.Page
    {
        protected bool Validated = true;
        Dictionary<string, List<string>> Cities = new Dictionary<string, List<string>>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Table MainList = new Table();
            MainList.ID = "MainList";
            ScriptManager Manager = new ScriptManager();
            Manager.ID = "Manager";
            UpdatePanel Panel = new UpdatePanel();
            Panel.ID = "Panel";
            Panel.UpdateMode = UpdatePanelUpdateMode.Always;
            Panel.ContentTemplateContainer.Controls.Add(MainList);
            Form.Controls.Add(Manager);
            Form.Controls.Add(Panel);

            TableRow Row = new TableRow();
            Row.ID = "NickRow";
            TableCell cell = new TableCell();
            Label Label = new Label();
            Label.Text = "Nick Name:";
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            cell = new TableCell();
            TextBox Box = new TextBox();
            Box.AutoPostBack = true;
            Box.ID = "NickBox";
            cell.Controls.Add(Box);
            Row.Cells.Add(cell);
            cell = new TableCell();
            CustomValidator NickValidator = new CustomValidator();
            NickValidator.ID = "NickValidator";
            NickValidator.Display = ValidatorDisplay.Dynamic;
            NickValidator.ServerValidate += ValidateNickName;
            cell.Controls.Add(NickValidator);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Row = new TableRow();
            Row.ID = "NameRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "First Name:";
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            cell = new TableCell();
            Box = new TextBox();
            Box.AutoPostBack = true;
            Box.ID = "NameBox";
            cell.Controls.Add(Box);
            Row.Cells.Add(cell);
            cell = new TableCell();
            RequiredFieldValidator Validator = new RequiredFieldValidator();
            Validator.ID = "NameValidator";
            Validator.ControlToValidate = "NameBox";
            Validator.ErrorMessage = "Name field should not be empty.";
            //Validator.ValidationGroup = "default";
            cell.Controls.Add(Validator);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Row = new TableRow();
            Row.ID = "LastNameRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "Last Name:";
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            cell = new TableCell();
            Box = new TextBox();
            Box.AutoPostBack = true;
            Box.ID = "LastNameBox";
            cell.Controls.Add(Box);
            Row.Cells.Add(cell);
            cell = new TableCell();
            //Validator = new RequiredFieldValidator();
            //Validator.ID = "LastNameValidator";
            //Validator.ControlToValidate = "LastNameBox";
            //Validator.ErrorMessage = "Last Name field should not be empty.";
            CustomValidator LastNameValidator = new CustomValidator();
            LastNameValidator.ID = "LastNameValidator";
            LastNameValidator.Display = ValidatorDisplay.Dynamic;
            LastNameValidator.ServerValidate += ValidateLastName;
            cell.Controls.Add(LastNameValidator);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Row = new TableRow();
            Row.ID = "DateRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "Date of birth:";
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            cell = new TableCell();
            Box = new TextBox();
            Box.AutoPostBack = true;
            Box.ID = "DateBox";
            cell.Controls.Add(Box);
            Calendar DateOfBirth = new Calendar();
            DateOfBirth.SelectionChanged += DateOfBirth_SelectionChanged;
            DateOfBirth.ID = "DateOfBirth";
            cell.Controls.Add(DateOfBirth);
            Row.Cells.Add(cell);
            cell = new TableCell();
            Validator = new RequiredFieldValidator();
            Validator.ID = "DateRequiredValidator";
            Validator.ControlToValidate = "DateBox";
            Validator.ErrorMessage = "Date of birth field should not be empty.";
            cell.Controls.Add(Validator);
            RangeValidator DateValidator = new RangeValidator();
            DateValidator.Type = ValidationDataType.Date;
            DateValidator.MinimumValue = new DateTime(1960, 1, 1).ToShortDateString();
            DateValidator.MaximumValue = DateTime.Now.ToShortDateString();
            DateValidator.ID = "BirthdayValidator";
            DateValidator.ControlToValidate = "DateBox";
            DateValidator.ErrorMessage = "Don't lie about your birthday, you're not trying to impress me!";
            cell.Controls.Add(DateValidator);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Row = new TableRow();
            Row.ID = "EmailRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "Email:";
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            cell = new TableCell();
            Box = new TextBox();
            Box.AutoPostBack = true;
            Box.ID = "EmailBox";
            cell.Controls.Add(Box);
            Row.Cells.Add(cell);
            cell = new TableCell();
            Validator = new RequiredFieldValidator();
            Validator.ID = "EmailRequiredValidator";
            Validator.ControlToValidate = "EmailBox";
            Validator.ErrorMessage = "Email field should not be empty.";
            cell.Controls.Add(Validator);
            RegularExpressionValidator EmailValidator = new RegularExpressionValidator();
            EmailValidator.ID = "EmailValidator";
            EmailValidator.ControlToValidate = "EmailBox";
            EmailValidator.ValidationExpression = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            EmailValidator.ErrorMessage = "Enter a correct email.";
            cell.Controls.Add(EmailValidator);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Cities.Add("USA", new List<string>());
            Cities["USA"].Add("Boston");
            Cities["USA"].Add("Detroit");
            Cities["USA"].Add("New York");
            Cities["USA"].Add("Miami");
            Cities.Add("Ukarine", new List<string>());
            Cities["Ukarine"].Add("Kyiv");
            Cities["Ukarine"].Add("Kharkiv");
            Cities["Ukarine"].Add("Lviv");
            Cities.Add("China", new List<string>());
            Cities["China"].Add("Hong-Kong");
            Cities["China"].Add("Beijing");

            Row = new TableRow();
            Row.ID = "CountryRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "Your country:";
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            cell = new TableCell();
            DropDownList List = new DropDownList();
            List.AutoPostBack = true;
            List.ID = "CountryBox";
            List.DataSource = Cities.Keys;
            List.DataBind();
            cell.Controls.Add(List);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Row = new TableRow();
            Row.ID = "CityRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "Your city:";
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            cell = new TableCell();
            List = new DropDownList();
            List.AutoPostBack = false;
            List.ID = "CityBox";
            cell.Controls.Add(List);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Row = new TableRow();
            Row.ID = "SummaryRow";
            cell = new TableCell();
            Label = new Label();
            Label.Text = "Summary:";
            cell.Controls.Add(Label);
            Row.Cells.Add(cell);
            cell = new TableCell();
            ValidationSummary Summary = new ValidationSummary();
            Summary.DisplayMode = ValidationSummaryDisplayMode.BulletList;
            Summary.ID = "Summary";
            cell.Controls.Add(Summary);
            Row.Cells.Add(cell);
            MainList.Rows.Add(Row);

            Button Submit = new Button();
            Submit.Click += Submit_Click;
            Submit.Text = "Submit";
            Button Refresh = new Button();
            Refresh.Click += Refresh_Click;
            Refresh.Text = "Refresh";
            Form.Controls.Add(Submit);
            Form.Controls.Add(Refresh);
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            Page.Response.Redirect(Page.Request.Url.AbsoluteUri);
        }

        private void Submit_Click(object sender, EventArgs e)
        {
        }

        private void ValidateNickName(object source, ServerValidateEventArgs args)
        {
            TextBox NickBox = (TextBox)Form.FindControl("NickBox");
            TextBox NameBox = (TextBox)Form.FindControl("NameBox");
            CustomValidator NickValidator = (CustomValidator)Form.FindControl("NickValidator");
            if (NickBox.Text.Equals(String.Empty))
            {
                NickValidator.ErrorMessage = "Nickname field should not be empty.";
                args.IsValid = false;
            }
            else if (NickBox.Text.Equals(NameBox.Text))
            {
                NickValidator.ErrorMessage = "Nickname != Name.";
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        private void ValidateLastName(object source, ServerValidateEventArgs args)
        {
            TextBox LastNameBox = (TextBox)Form.FindControl("LastNameBox");
            TextBox NameBox = (TextBox)Form.FindControl("NameBox");
            CustomValidator NickValidator = (CustomValidator)Form.FindControl("LastNameValidator"); 
            if (LastNameBox.Text.Equals(String.Empty))
            {
                NickValidator.ErrorMessage = "LastName field should not be empty.";
                args.IsValid = false;
            }
            else if (LastNameBox.Text.Equals(NameBox.Text))
            {
                NickValidator.ErrorMessage = "LastName != Name.";
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        private void DateOfBirth_SelectionChanged(object sender, EventArgs e)
        {
            TextBox DateBox = (TextBox)Form.FindControl("DateBox");
            var Date = (Calendar)sender;
            DateBox.Text = Date.SelectedDate.ToShortDateString();
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            DropDownList CountryBox = (DropDownList)Form.FindControl("CountryBox");
            DropDownList CityBox = (DropDownList)Form.FindControl("CityBox");
            if ((List<string>)CityBox.DataSource != Cities[CountryBox.SelectedValue])
            {
                CityBox.DataSource = Cities[CountryBox.SelectedValue];
                CityBox.DataBind();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SharePointASP
{
    public partial class Default : System.Web.UI.Page
    {
        private void Page_PreInit(object sender, EventArgs e)
        {
            Page.Response.Write("Page.Pre_Init event handled at " + DateTime.Now.ToString("hh:mm:ss") + "<br />");
        }

        private void Page_Init(object sender, EventArgs e)
        {
            Page.Response.Write("Page.Init event handled at " + DateTime.Now.ToString("hh:mm:ss") + "<br />");
        }

        private void Page_InitComplete(object sender, EventArgs e)
        {
            Page.Response.Write("Page.InitComplete event handled at " + DateTime.Now.ToString("hh:mm:ss") + "<br />");
        }

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            Page.Response.Write("Page.PreLoad event handled at " + DateTime.Now.ToString("hh:mm:ss") + "<br />");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Response.Write("Page.Load event handled at " + DateTime.Now.ToString("hh:mm:ss") + "<br />");
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            Page.Response.Write("Page.LoadComplete event handled at " + DateTime.Now.ToString("hh:mm:ss") + "<br />");
        }
        
        private void Page_PreRender(object sender, EventArgs e)
        {
            Page.Response.Write("Page.Pre_Render event handled at " + DateTime.Now.ToString("hh:mm:ss") + "<br />");
        }

        private void Page_PreRenderComplete(object sender, EventArgs e)
        {
            Page.Response.Write("Page.Pre_RenderComplete event handled at " + DateTime.Now.ToString("hh:mm:ss") + "<br />");
        }

        private void Page_SaveStateComplete(object sender, EventArgs e)
        {
            Page.Response.Write("Page.SaveStateComplete event handled at " + DateTime.Now.ToString("hh:mm:ss") + "<br />");
            foreach (string key in Page.Request.Form.AllKeys)
            {
                TableRow RowToAdd = new TableRow();
                TableHeaderCell propertyCell = new TableHeaderCell();
                propertyCell.Text = key;
                RowToAdd.Cells.Add(propertyCell);
                propertyCell = new TableHeaderCell();
                propertyCell.Text = Page.Request.Form[key].ToString();
                RowToAdd.Cells.Add(propertyCell);
                Table1.Rows.Add(RowToAdd);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Page.Response.Write("Button1.Click event handled at " + DateTime.Now.ToString("hh:mm:ss") + "<br />");
        }
    }
}
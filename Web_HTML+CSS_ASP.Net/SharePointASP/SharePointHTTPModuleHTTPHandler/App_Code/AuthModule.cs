﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SharePointHTTPModuleHTTPHandler
{
    public class AuthModule : IHttpModule
    {
        public void Dispose()
        {
            // Nothing to do here
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest +=
            (new EventHandler(this.Application_BeginRequest));
        }

        private void Application_BeginRequest(Object source,
         EventArgs e)
        {
            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;
            if (!(context.Request.QueryString["UserKey"] == null))
            {
                string ID = context.Request.QueryString["UserKey"];
                var Decoder = MD5.Create();
                bool ValidUser = VerifyMd5Hash(Decoder, "I_Am_Real_Admin", ID);
                if (!ValidUser)
                {
                    context.Response.Write("MD5, motherfucker, do you have it?!");
                    context.Response.Flush();
                    context.Response.End();
                }
            }
            else
            {
                context.Response.Write("MD5, motherfucker, do you have it?!");
                context.Response.Flush();
                context.Response.End();
            }
        }
        
        private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        private static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            string hashOfInput = GetMd5Hash(md5Hash, input);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
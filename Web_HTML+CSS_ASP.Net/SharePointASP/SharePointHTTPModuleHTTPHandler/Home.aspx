﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="SharePointHTTPModuleHTTPHandler.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:linkbutton runat="server" OnClick="Upload_Click">Upload Image</asp:linkbutton>
        <asp:DropDownList ID="ImageList" runat="server" AutoPostBack="False"></asp:DropDownList>
        <asp:Button ID="LoadButton" runat="server" Text="Load" OnClientClick ="document.forms[0].target = '_blank';"/>
    </div>
    </form>
</body>
</html>

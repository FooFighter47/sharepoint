﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="SharePointHTTPModuleHTTPHandler.Upload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:FileUpload ID="PictureUpload" runat="server" OnDataBinding="PictureUpload_DataBinding" />
        <asp:Button ID="UploadButton" runat="server" Text="Button" OnClick="UploadButton_Click" />
        <asp:Label ID="FileID" runat="server" Text=""></asp:Label>
    </div>
    </form>
</body>
</html>

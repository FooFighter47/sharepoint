﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;

namespace SharePointHTTPModuleHTTPHandler
{
    public class WatermarkHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            var Decoder = MD5.Create();
            var PicUrl = context.Request.Url;
            string temp = PicUrl.AbsolutePath.Replace("Images", "temp");
            string tempFolder = temp.Replace(Path.GetFileName(temp), "");
            if (!Directory.Exists(context.Server.MapPath(tempFolder)))
                Directory.CreateDirectory(context.Server.MapPath(tempFolder));
            var Picture = Bitmap.FromFile(context.Server.MapPath(PicUrl.AbsolutePath));
            string MarkUrl = "/SharePointHTTPModuleHTTPHandler/watermark.png";
            var Watermark = Bitmap.FromFile(context.Server.MapPath(MarkUrl));
            double scale = Convert.ToDouble(Watermark.Width) / Convert.ToDouble(Watermark.Height);
            Graphics Mark = Graphics.FromImage(Picture);
            Mark.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
            Mark.DrawImage(Watermark, Picture.Width/2 - Convert.ToInt32((Picture.Height * scale) / 2), 0, Convert.ToInt32(Picture.Height * scale), Picture.Height);
            Mark.Save();
            Mark.Dispose();
            Bitmap Final = new Bitmap(Picture);
            Final.Save(context.Server.MapPath(temp));
            Picture.Dispose();
            Final.Dispose();
            context.Response.Redirect(temp + "?UserKey=" + GetMd5Hash(Decoder, "I_Am_Real_Admin"));
        }

        private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }
}
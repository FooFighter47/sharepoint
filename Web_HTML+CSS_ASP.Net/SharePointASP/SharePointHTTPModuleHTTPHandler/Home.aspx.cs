﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;

namespace SharePointHTTPModuleHTTPHandler
{
    public partial class Home : System.Web.UI.Page
    {
        MD5 Decoder;
        protected void Page_Load(object sender, EventArgs e)
        {
            Decoder = MD5.Create();
            if (!Directory.Exists(Server.MapPath("Images")))
                Directory.CreateDirectory(Server.MapPath("Images"));
            else
            {
                var Files = Directory.EnumerateFiles(Server.MapPath("Images")).ToList();
                List<string> ImageNames = new List<string>();
                foreach (string image in Files)
                {
                    ImageNames.Add(Path.GetFileName(image));
                }
                Files = null;
                if (ImageList.DataSource == null || !ImageList.DataSource.Equals(ImageNames))
                {
                    if (!IsPostBack)
                    {
                        ImageList.DataSource = ImageNames;
                        ImageList.DataBind();
                    }
                }
            }
            LoadButton.Click += LoadButton_Click;
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
                var r = Request.Url;
                ImageList.DataBind();
                if (Context.Items.Contains("Picture"))
                    Context.Items.Remove("Picture");
                Response.Redirect("Images/" + ImageList.SelectedValue + "?UserKey=" + GetMd5Hash(Decoder, "I_Am_Real_Admin"));
        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            Response.Redirect("/SharePointHTTPModuleHTTPHandler/Upload.aspx?UserKey=" + GetMd5Hash(Decoder, "I_Am_Real_Admin"));
        }

        private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }
}
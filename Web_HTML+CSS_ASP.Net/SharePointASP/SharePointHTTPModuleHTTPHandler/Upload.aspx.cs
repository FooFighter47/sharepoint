﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace SharePointHTTPModuleHTTPHandler
{
    public partial class Upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (PictureUpload.FileName.Equals(String.Empty))
            {
                FileID.Text = "Select file first!";
            }
            else if (!Regex.IsMatch(PictureUpload.FileName, "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)"))
            {
                FileID.Text = "Select image file!";
            }
            else
            {
                try
                {
                    string FileName = "Images/";
                    FileName += GetUniqueName(Server.MapPath(FileName));
                    FileName += Path.GetExtension(PictureUpload.FileName);
                    PictureUpload.SaveAs(Server.MapPath(FileName));
                    FileID.Text = "Your file ID: " + Path.GetFileName(FileName);
                }
                catch (OutOfMemoryException ex)
                {
                    FileID.Text = "Selected image is corrupted!";
                }
            }
        }

        protected void PictureUpload_DataBinding(object sender, EventArgs e)
        {
            
        }

        private string GetUniqueName(string directory)
        {
            Random r = new Random();
            string Next = r.Next(0, 100000).ToString();
            string Dir = directory;
            bool Unique = true;
            foreach (string Image in Directory.GetFiles(Dir))
            {
                if (Image.Contains(Next))
                {
                    Unique = false;
                    break;
                }
            }
            if (Unique)
                return Next;
            else
                return GetUniqueName(Dir);
        }
    }
}